job('Kim Job to Push to Dockerhub') {
    scm {   
        git{
            remote{
                credentials('Git')
                url("https://WoonKim@bitbucket.org/WoonKim/my_jenkins.git")
            }
        }     
    }
    triggers {
        scm('H/5 * * * *')
    }
    wrappers {
        nodejs('nodejs') // this is the name of the NodeJS installation in 
                         // Manage Jenkins -> Configure Tools -> NodeJS Installations -> Name
    }
    steps {
        dockerBuildAndPublish {
            repositoryName('woonkim/auto_come')
            tag('${GIT_REVISION,length=9}')
            registryCredentials('dockerhub')
            forcePull(false)
            forceTag(false)
            createFingerprints(false)
            skipDecorate()
        }
    }
}